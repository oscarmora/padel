<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:2|max:30',
            'lastname'  => 'min:2|max:30',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|min:8|max:30',
            'brithdate' => 'date',
            'handedness'=> [Rule::in(['R', 'L', 'A'])],
            'level'     => [Rule::in([1, 2, 3, 4, 5])],
            'cellPhone' => 'max:20',
            'type' => ['required', Rule::in(['G', 'M', 'A'])],
        ];
    }
}
