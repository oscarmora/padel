<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);
            $table->string('lastname', 30)->nullable();
            $table->string('profileImage', 30)->nullable();
            $table->integer('country_id');
            $table->string('email',100)->unique();
            $table->string('password',64);
            $table->string('cellphone', 20);
            $table->enum('gender', ['F','M']);
            $table->enum('level', [1,2,3,4,5]);
            $table->date('birthday');
            $table->enum('handedness', ['R','L','A'])->nullable(); //R = Derecho, L = Zurdo, A =  Ambidiestro
            $table->enum('type', ['M','G','A'])->nullable(); //M =  Miembro, G = Invitado, A = Admin
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
